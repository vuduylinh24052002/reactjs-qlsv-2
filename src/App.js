import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import UserContextProvider from './context/AppContext';
import {BrowserRouter , Routes, Route} from 'react-router-dom'
import Home from './components/Home';
import AddUser from './components/AddUser';
import EditUser from './components/EditUser';

function App() {
  return (
    <UserContextProvider>
      <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/add-user' element={<AddUser />}/>
        <Route path='/edit-user/:id' element={<EditUser />}/>
      </Routes>
      </BrowserRouter>
    </UserContextProvider>

  );
}

export default App;
