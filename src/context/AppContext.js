import React, { createContext, useState } from 'react';
import axios from 'axios';

export const AppContext = createContext();

const UserContextProvider = ({ children }) => {
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    try {
      const response = await axios.get('https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users');
      setUsers(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const addUser = async (user) => {
    try {
      const response = await axios.post('https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users', user);
      setUsers([...users, response.data]);
    } catch (error) {
      console.log(error);
    }
  };

  const editUser = async (user) => {
    try {
      const response = await axios.put(`https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users/${user.id}`, user);
      const updatedUsers = users.map((u) => (u.id === user.id ? response.data : u));
      setUsers(updatedUsers);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteUser = async (id) => {
    try {
      await axios.delete(`https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users/${id}`);
      const filteredUsers = users.filter((user) => user.id !== id);
      setUsers(filteredUsers);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <AppContext.Provider value={{ users, getUsers, addUser, editUser, deleteUser }}>
      {children}
    </AppContext.Provider>
  );
};

export default UserContextProvider;
