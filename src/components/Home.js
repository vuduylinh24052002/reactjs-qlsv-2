import React, {useContext, useEffect} from "react";
import {Link} from 'react-router-dom'
import { AppContext } from "../context/AppContext";

const Home = () => {
    const { users, deleteUser} = useContext(AppContext);
    const {getUsers} = useContext(AppContext);
    useEffect(() => {
      getUsers()
    }, []);
    const handleDelete = async (id) =>{
        if (window.confirm('Are you sure to delete this user?')) {
            deleteUser(id);
          }
    }

return(
    <div className='container'>
      <h1 className="text-center">User List</h1>
      <div className="d-flex justify-content-end mb-3">
        <Link to="/add-user" className="btn btn-primary" >
          Add User
        </Link>
        
      </div>
      <table className="table table-bordered table-striped" >
        <thead style={{backgroundColor : '#4CAF50', color  : 'white'}}>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.length > 0 ? (
            users.map((user) => (
              <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.phone}</td>
                <td>{user.address}</td>
                <td>
                  <Link to={`/edit-user/${user.id}`} className="btn btn-success me-2"> Edit </Link>
                  <button className="btn btn-danger" onClick={() => handleDelete(user.id)}> Delete </button>
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="5" className="text-center">
                No users found.
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
)
};

export default Home;


