import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AppContext } from '../context/AppContext';

const AddUser = () => {
  const { addUser } = useContext(AppContext);
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    const user = {
      name: name,
      phone: phone,
      address: address,
    };
    addUser(user);
    navigate('/');
  };

  return (
    <div className='container'>
      <h2 className='text-center'>Add User</h2>
      <form onSubmit={handleSubmit} style={{marginTop:50}}>

        <div className='form-group' style={{marginTop:20}}>
        <label>Name:</label>
        <input
          type="text" 
          value={name} 
          onChange={(e) => setName(e.target.value)}  
          className='form-control' 
          required
         />
        </div>

        <div className='form-group' style={{marginTop:20}}>
        <label>Phone:</label>
        <input
          type="text"
          value={phone} onChange={(e) => setPhone(e.target.value)} 
          className='form-control' 
          required
        />
        </div>

        <div className='form-group' style={{marginTop:20}}>
        <label>Address:</label>
          <input 
            type="text" 
            value={address} onChange={(e) => setAddress(e.target.value)} 
            className='form-control' 
            required
          />
        </div>
        
        <button type="submit" className="btn btn-primary" style={{marginTop : 20}}>Add</button>
      </form>
    </div>
  );
};

export default AddUser;
