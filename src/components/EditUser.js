import React, { useContext, useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { AppContext } from '../context/AppContext';
import axios from 'axios';


const EditUser = () => {
  const { id } = useParams();
  const { users,getUsers, editUser } = useContext(AppContext);
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const user = users.find((user) => user.id === id);
   
    if (user) {
    setName(user.name);
    setPhone(user.phone);
    setAddress(user.address);
    console.log('....' ,user)
    }
  }, [id,users]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const UpdateUser = {
      id: id,
      name: name,
      phone: phone,
      address: address,
    };
    editUser(UpdateUser);
    navigate('/');
  };

  return (
    <div className='container'>
      <h2 className='text-center'>Edit User</h2>
      <form onSubmit={handleSubmit} style={{marginTop:50}}>

        <div className='form-group' style={{marginTop:20}}>
        <label>Name:</label>
          <input 
          type="text" 
          value={name} onChange={(e) => setName(e.target.value)} 
          className='form-control' 
          required/>
        </div>

        <div className='form-group' style={{marginTop:20}}>
        <label> Phone:</label>
          <input 
          type="text" 
          value={phone} onChange={(e) => setPhone(e.target.value)} 
          className='form-control' 
          required/>
        </div>

        <div className='form-group' style={{marginTop:20}}>
        <label>Address:</label>
          <input 
          type="text" 
          value={address} onChange={(e) => setAddress(e.target.value)} 
          className='form-control' 
          required/>
        </div>

        <button type="submit" className="btn btn-success" style={{marginTop : 20}}>Save</button>
      </form>
    </div>
  );
};

export default EditUser;
